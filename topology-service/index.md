# Topology Service

The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.
  - https://gitlab.com/gitlab-com/runbooks/-/merge_requests/7643

### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
  - Topology service is deployed using Runway https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/299 all infrastructure is in code
- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?
  - GCP load balancers are used, but DDoS protection is not enforced. We use the default [concurrency limits](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/e73919caead02b096d846045a3cada6d38083938/schemas/service-manifest/v1/manifest.schema.json#L357), and default [max_instances](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/e73919caead02b096d846045a3cada6d38083938/schemas/service-manifest/v1/manifest.schema.json#L349), however, a single user can still saturate all the connections.
- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?
  - Yes Runway tags resources using the defined standards

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] List the top three operational risks when this feature goes live.
  - Unbounded costs since it's a service that is publicly available and not heavily rate-limited. We have the default [`max_instances`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/e73919caead02b096d846045a3cada6d38083938/schemas/service-manifest/v1/manifest.schema.json#L349) and default [`max_instance_request_concurrency`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/e73919caead02b096d846045a3cada6d38083938/schemas/service-manifest/v1/manifest.schema.json#L357) so we only serve 800 requests concurrency.
    [Calculating](https://cloud.google.com/products/calculator?hl=en&dl=CiQxMmUyYTNjZS0zM2U4LTRlZjEtYTcwMC0xMGJhOWJhYzJhN2QQHBokMUJDRDM3OTYtNEUxOC00RkM2LUFDNjEtRDM0Njk1OURFQkJF) 10 million requests per month it turns out really cheap as well.
  - Topology service deployment is not presently mirrored to ops.gitlab.net if gitlab.com is down deployments and rollbacks can not be triggered. However, if the topology service is down it will prevent engineers from accessing the Cell we route to. The Cell will not have any customer data on it, and customers will not be affected.
  - No authorization and authentication, only static public information is returned at present
- [ ] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?
  - Topology service blast radius should be limited to the HTTP Router only in the experimental phase. If HTTP Router can't reach topology service it will not be able to route the request to the Cell, but will still be able to send requests to the existing GitLab.com deployment.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service
  - Will be worked on here: https://gitlab.com/gitlab-org/cells/topology-service/-/issues/17

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - Changes to the topology service itself during the experiment will be done in the cells environment and deployed automatically on merge to main 
  [here](https://gitlab.com/gitlab-org/cells/topology-service/-/issues/11) Changes to HTTP Router that will leverage the topology service will be rolled out using
  change management issues.
- [ ] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?
  - It can be rolled back by reverting a commit, but feature flags are not yet supported
- [ ] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).
  - https://gitlab.com/gitlab-org/cells/topology-service pipeline is building container images for this service.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [ ] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:
    - gitlab-runway-topo-svc-stg in gitlab-cells.dev (GCP)
    - gitlab-runway-topo-svc-prod in gitlab-cells (GCP)
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
    - topo-svc.staging.runway.gitlab.net. and topo-svc.runway.gitlab.net.
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
    - https://topology-rest-55gfgv.staging.runway.gitlab.net
    - production url will be generated after readiness review is accepted
  - Other (anything relevant that might be worth mention):
- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
  - Yes, [here](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/threat-models/-/blob/master/gitlab-org/cells/README.md?ref_type=heads)
- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
  - TODO: Renovate [here](https://gitlab.com/gitlab-org/cells/topology-service/-/issues/28)
- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.
  - Yes, IAC is managed inside runway using standard.yml and terraform.yml templates found [here](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/tree/main/templates?ref_type=heads)
- [ ] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?
  - Images are not currently scanned will be worked on as part of issue [here](https://gitlab.com/gitlab-org/cells/topology-service/-/issues/29)

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?
    - No, initially service will be internal only, secure authentication between services is still under discussion [here](https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/7160)
- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?
    - Yes, new cells infrastructure projects are provisioned following that principle see [here](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/328)
- [ ] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
    - Services are restricted to ingress based on their defined container port and protocol egress is not restricted
- [ ] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?
    - No
### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?
  - No extra logging beyond what container is logging on startup at the moment.
    We are considering adding logging in [this issue](https://gitlab.com/gitlab-org/cells/topology-service/-/issues/16)
- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
  - At present no data is stored other than the configuration files found in https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer
- [ ] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?
  - Yellow, At present no data is stored and only information specific to the infrastructure configuration is transmitted
- [ ] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
  - Load balancers store access logs, any other request logging is handled by the application [this issue](https://gitlab.com/gitlab-org/cells/topology-service/-/issues/16)
 - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
 - [ ] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

```