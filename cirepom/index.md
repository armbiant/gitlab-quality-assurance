# CIrepoM

State: **WIP**

[CIrepoM**](https://gitlab.com/gitlab-com/gl-infra/cirepom) is a tool designed to automatically perform large numbers of repository migrations on CI pipelines automatically. Manual intervention is only required to:

* provide **CIrepoM** with the list of repository that comprise a migration, and 
* investigate migration failures, which are reported through issues periodically

#### Dogfooding

[We use our own product](https://about.gitlab.com/handbook/values/#dogfooding), and **CIrepoM** provides important functionality relevant to the ongoing upkeep of large GitLab instances with multiple backend repository storage shards. Our ultimate goal is to see this functionality in GitLab itself.

**CIrepoM** was implemented to meet tight deadlines related to ongoing storage backend rebalancing and, more pressingly, to infrastructure cost management initiatives. These are critical to GitLab.com's availability and to the company's financials. The next iteration will be to start moving CIrepoM functionality into GitLab itself. One important aspect is our ability, in the near future, to take advantage of new API functionality for more reliable repository migrations.

## Table of contents

  * [Architecture overview](#architecture-and-overview)
  * [Configuration](#configuration)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Security considerations](#security-considerations)
  * [Application upgrade/rollback](#application-upgrade-and-rollback)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)

## Architecture Overview

**CIrepoM** is a simple system with three main components:

* a GitLab instance hosting the `cirepom-bot` repository and executes migrations on CI/CD pipelines
* a target GitLab instance that hosts the repositories to be migrated
* a Firestore database, where migration metadata is stored



![cirepom_architecture-0.0.1](./img/cirepom_architecture-0.0.1.png)





## Configuration

The `cirepom-bot` repository contains the necessary data to execute repository migrations.

Each target GitLab instance has a permanent branch in this repository, and MRs emrged against said branches will trigger pipeline executions.









#### `Project` State Machine

![Project State Machine](./img/cirepom_project_statemachine-0.0.1.png)







```mermaid
stateDiagram

	state "project_exists?" as loaded
	state "repository_storage_validated?" as waiting
	state "api_migration_runnning?" as running
	state "INVESTIGATION" as quarantined
	state "repository_verified" as transferred

    [*] --> loaded
    
    state project_exists <<fork>>
      loaded --> project_exists: fetch!
      project_exists --> waiting: true
      project_exists --> quarantined: false
      
      state repository_storage_validated <<fork>>
        waiting --> repository_storage_validated: dispatch!
        repository_storage_validated --> running: true
        repository_storage_validated --> quarantined: false
        
        state api_migration_completed <<fork>>
          running --> api_migration_completed: false
          api_migration_completed --> transferred: true
          api_migration_completed --> quarantined: false
     
     state repository_verified <<fork>
        transferred --> repository_verified: true
        repository_verified --> verified
        repository_verified --> quarantined
        
      state api_migration_runnning <<fork>>
        running --> api_migration_runnning
        api_migration_runnning --> running: true
        api_migration_runnning --> api_migration_completed: false
        
      quarantined --> loaded: reload!
      
      verified --> [*]
          
      
        
        

```

## Configurati