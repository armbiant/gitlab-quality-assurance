_While it is encouraged for parts of this document to be filled out,
not all of the items below will be relevant. Leave all non-applicable
items intact and add the reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but
should be, it is strongly encouraged to add it as necessary._

## Summary

- [ ] **Provide a high level summary of this new product
  feature. Explain how this change will benefit GitLab
  customers. Enumerate the customer use-cases.**

    Customers in GitLab SaaS Premium and Ultimate will be able to
    execute macOS CI/CD jobs without needing to set up and maintain a
    macOS build environment and macOS Runner.

    There are other compelling reasons to add this feature to our SaaS
    offering from a business perspective. First, our primary
    competitors have offered SaaS-hosted macOS CI/CD build
    environments for some time. More importantly, there are signals
    in the market that a critical competitive vector long-term will be
    on offering a comprehensive portfolio of hosted build environment
    resource options. For example, CircleCI's positioning is that
    offering any machine type to support builds in any language is
    what sets apart their hosted CI solution. And now, GitHub is
    following suit by
    [expanding](https://github.com/github/roadmap/issues/161) the
    resource classes offered in their SaaS offering.

    So while I propose that we (GitLab) won't compete head-on
    regarding the [multiple resource
    classes](https://circleci.com/product/features/resource-classes/)
    or [speed](https://circleci.com/circleci-versus-github-actions/)
    vectors and instead focus on security and ease of use, we must
    close gaps such as macOS resource classes before that starts
    impacting customer retention.
  
- [ ] **What metrics, including business metrics, should be monitored
  to ensure will this feature launch will be a success?**

	TODO: update

    In terms of quantifiable business impact, we expect the
    incremental revenue from macOS CI minutes to be non-material over
    the next 12 to 18 months. However, offering a macOS SaaS solution
    supports retaining the business of accounts valued at $3 to $5
    million annual recurring revenue. The current forecast indicates
    that ~350 customers in Premium and Ultimate will be using the
    offer within the first year post-launch and executing on average
    95,000 CI jobs per month, consuming 500k CI minutes.

    **Business Metrics Forecast Table**

    |                | May 2022 | Jun 2022 | Jul 2022 | Aug 2022 | Sep 2022 | Oct 2022 |
    | -------------- | -------- | -------- | -------- | -------- | -------- | -------- |
    | Customer count | 165      | 176      | 187      | 199      | 211      | 225      |
    | CI jobs        | 49,500   | 52,650   | 56,007   | 59,585   | 63,398   | 67,463   |
    | CI minutes     | 247,500  | 263,250  | 280,035  | 297,924  | 316,992  | 337,317  |

    **Application metrics**

    As for the application metrics, we're working on including macOS
    Runner Managers in our monitoring stack just like all the other
    runner managers we have. With that, we will use exactly the same
    metrics and dashboards as for the rest of the SaaS Runners.

    - [CI Runners:
      overview](https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview)
    - [Deployment
      overview](https://dashboards.gitlab.net/d/ci-runners-deployment/ci-runners-deployment-overview)
    - [Incident support:
      database](https://dashboards.gitlab.net/d/ci-runners-incident-database/ci-runners-incident-support-database)
    - [Incident support: GitLab
      Application](https://dashboards.gitlab.net/d/ci-runners-incident-gitlab-application/ci-runners-incident-support-gitlab-application)
    - [Incident support: runner
      manager](https://dashboards.gitlab.net/d/ci-runners-incident-runner-manager/ci-runners-incident-support-runner-manager)

## Architecture

- [ ] **Add architecture diagrams to this issue of feature components
  and how they interact with existing GitLab components. Make sure to
  include the following: Internal dependencies, ports, encryption,
  protocols, security policies, etc.**

	![Mac Runner Architecture Diagram](SaaS MacOS runners in AWS - general architecture overview.drawio.png)

- [ ] **Describe each component of the new feature and enumerate what
  it does to support customer use cases.**

	Mac runner managers are hosted in GCP alongside Linux runner
	managers. However runner jobs are executed in AWS on dedicated
	hosts. So there is infrastructure on both providers and a VPN
	tunnel between. All components below of configured via
	Terraform templates.

	- New Instances: GCP Mac Runner Managers
	
	Jobs are picked up from gitlab.com by saas macos runner managers
	running in GCE. These are configured and integrated just like the
	Linux runner managers. Macos runner managers are provisioned in
	the existing `ci` VPC which is peered with the existing `gprd`
	VPC. The existing Prometheus server in `gprd` scrapes metrics from
	the macos runner managers in `ci`, just like Linux runners. Macos
	runner managers also send their logs to cloud logging, just like
	Linux runner managers.

	However instead of running jobs in GCE VMs, macos runner managers
	run jobs in AWS VMs over a dedicated VPN tunnel.

	- New Component: GCP-AWS VPN Tunnel

	Runner manger in GCP and ephermeral instances in AWS are connected
	via a VPC tunnel. This is configured as a Cloud VPN and Cloud
	Router on the GCP side and an AWS VPC and Route Table on the AWS
	side.

    - New Feature: Runner Taskscaler and AWS Fleeting Plugin
	
	GitLab Runner is replacing Docker Machine with a new library
    called
    [Taskscaler](https://docs.gitlab.com/ee/architecture/blueprints/runner_scaling/index.html#taskscaler-provider)
    which uses a plugin system (Fleeting) to create ephermal VMs for
    running jobs. Taskscaler is an autoscaling which is responsible
	for creating capacity on demand, maintaining sufficient capacity
	overhead and removing capacity when it is not needed.
	
	Saas mac runners will use Taskscaler and the AWS
    fleeting plugin to create, use and destroy ephermal VMs in EC2 on
    Mac instances. Taskscaler and the AWS plugin are components
	of GitLab Runner components which have not previously been
    deployed to production.
	
	- New Component: AWS Autoscaling Group (ASG)
	
	There are two ASGs configured to use mac2.metal instances, one
	green and blue.  Autoscaling is *not* enabled on the ASGs; runner
	manager takes care of scaling so the ASG is just like the GCP
	Managed Instance Group (MIG). The ASG has a configured minimum
	(0), maximum and desired number of instances. Runner manager will
	adjust the desired number of instances through a Fleeting
	plugin. Instances created by the ASG are automatically scheduled
	onto mac dedicated hosts.

	- New Component: AWS Dedicated Hosts

	The ASGs create dedicated hosts on-demand through a host resource
	group. Dedicated hosts are mac2.metal which are 8-core M1 mac
	minis. Dedicated hosts are the unit of compute cost for mac
	runners. EC2 schedules instances onto dedicated hosts, but the
	instances themselves cost nothing. Mac hardward on AWS is
	currently only available as dedicated hosts.
	
	Mac dedicated hosts come with some limitations. To comply with
	Apple's license restrictions, there is a 24-hour minimum
	allocation period. So dedicated hosts must be reused multiple
	times for mac runners to be economical. Additionally when
	instances are terminated from dedicated hosts there is a 40 minute
	scrubbing process of the underlying dedicated host during which
	the host is unavailable for instance placement. So instances must
	also be reused multiple times for mac runners to be
	economical. Therefore we run mac jobs in nested VMs within mac
	instances. Apple's license restrictions also limits the number of
	nested VMs to 2, which is enforced by their virtualization
	framework.
	
	- New Component: AWS S3 Cache Bucket
	
	Because jobs are running in AWS the runner cache should also
	be located in AWS to avoid cross-cloud network data charges.
	There is just one bucket shared between all blue/green runners,
	same as Linux runners.

- [ ] **For each component and dependency, what is the blast radius of
  failures? Is there anything in the feature design that will reduce
  this risk?**

    Any problems in this environment will affect only users who are
    executing jobs on the macOS SaaS Runners.

- [ ] **If applicable, explain how this new feature will scale and any
  potential single points of failure in the design.**

	Runner managers are single points of failure for jobs because they
	are individually responsible for dispatching jobs to ephermal VMs
	and returning the results back to GitLab (success/failure and
	logs). We mitigate this risk by using a blue/green deployment
	method to ensure there is always an operational runner manager
	during updates.
	
	Additionally there are 4 VPC tunnels between AWS and GCP so they
    can be brought down for maintenance one at a time.

## Operational Risk Assessment

- [ ] **What are the potential scalability or performance issues that
      may result with this change?**

	This feature scales in the same way as other hosted runners.
	There is a configured amount of idle capacity to absorb
	spikes in demand. And additional capacity is created as
	needed. The GitLab <-> Runner interactions are the same
	as other environments (Linux and Windows) so that aspect
	will scale in a known way.

	There is less known about the performance of AWS mac instances
	and how EC2 handles mac scalability. But we are in touch with
	AWS and they assure us they have sufficient capacity.

- [ ] **List the external and internal dependencies to the application
      (ex: redis, postgres, etc) for this feature and how the service
      will be impacted by a failure of that dependency.**

    - GCP Mac Runner Managers
	
	Mac runner manager failure will only impact mac jobs because
	other environments (Linux and Windows) have their own dedicated
	runner managers.
	
	- GCP-AWS VPN Tunnel

	Failure of the tunnel will prevent runner managers in GCP from
	connecting to ephermal VMs in AWS. No new Mac jobs will be started
	and no logs will be returned to GitLab. However runner managers
	will still be able to control the ASG.
	
	When the tunnel becomes available again, runner managers will
	not know the state of currently running jobs.

	When runners reboot or reconnect after a network partition event,
    they list the instances and connect to the nesting server on each
    one. If the nesting server is still up and running, they bring the
    instance into their capacity without terminating it. However
    runner does not current have the ability to reconnect to running
    jobs after reboot or connection drop. This is an existing runner
    limitiation, even for Linux runners.
	
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/issues/108

    - Runner Taskscaler and AWS Fleeting Plugin

	Taskscaler is an autoscaler capable of adding and removing
	capacity, both instances and nested VMs. Failure modes include
	under-provisioning, over-provisioning, thrashing and starvation.
	
	If Taskscaler removes too many instances or fails to increase
	instances in respond to demand (under-provisioning), mac jobs will
	enqueue and pipelines will be stuck. Bringing new instances online
	is quite slow compared to Linux, taking ~10s of minutes (versus
	1-2 minutes). So recovering from under-provisioning failure
	is also quite slow.
	
	If Taskscaler creates too many instances or fails to
	remove instances when they are not needed (over-provisioning)
	then the cost of mac runners to GitLab will be unnecessarily
	high. However user jobs will continue to run and pipelines
	will not be stuck. Recovering from overprovisioning can take
	up to 24 hours because of the minimum provisioning time of
	dedicated hosts.
	
	If Taskscaler creates and deletes instances too quickly (thrashing)
	most of the compute time and resources will be spent on
	initialization and cleanup. This will create an excessive number
	of dedicated hosts because many would be in scrubbing mode
	(40 minutes each) and therefore unable to host an instance.
	More dedicated hosts will be created in order to provision
	instances to run jobs. This failure mode could result in
	failed jobs, stuck jobs, excessive costs and other unstable
	system behaviors.
	
	If Taskscaler does not cleanup nested VMs within instances
	after jobs or after a disconnect failure, then no additional
	nested VMs can be created on a given instance (starvation).
	Macs support a maximum of 2 VMs per instance, so all
	instances could be non-viable because of zombie VMs. In this
	case Taskscaler will not provision more instances. Instead
	individual jobs will fail because there is no capacity in
	the target instances to start a VM.
	
	This is been addressed by
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/issues/109
	which will lazily delete VMs based on a slot system.

	- AWS Autoscaling Group
	
	If the ASG fails then runner managers will be able to 
	provision or remove instances. Additionally runner
	manager will be unable to discover existing instances
	because it uses ASG list for that purpose. However
	connections direcly to individual instances will not
	be affected so currently running jobs will complete.
	New jobs will not be started and autoscaling will not
	happen until the ASG comes back online.
	
	- AWS Dedicated Hosts
	
	If individual dedicated hosts fail then the instances
	assigned to them will fail, and the nested VMs within
	will fail. Runner jobs will fail and new jobs cannot
	be started.
	
	There is an additional configuration called a host resource
	resource group which created dedicated hosts on demand
	in response to instance creation. If this fails then
	the ASG will create instances but they will have no where
	to go since they must be scheduled onto a dedicated host.
	This will result in an under-provisioning failure.
	
	If the host resource does not release dedicated hosts
	when they are not needed then it will create unnecessary
	cost for GitLab.
		
	- AWS S3 Cache Bucket

	Runner cache is best-effort so if AWS S3 cache fails
	it will results is slower jobs because all cache
	requests will miss.
	
- [ ] **Were there any features cut or compromises made to make the
      feature launch?**

	We decided not to support Intel-based macs because Apple's
	virtualization framework, which does all the heavy lifting, only
	support the M1 architecture.

- [ ] **List the top three operational risks when this feature goes
      live.**

	- Taskscaler failure
	
	If our autoscaling system misbehaves then it could cause excessive
	cost to GitLab and/or failed jobs and stuck pipelines.
	
	- Unfamiliarity with AWS
	
	Most of our operational knowledge is centered around GCP.  So
	there are fewer people in the company that know how AWS works and
	how to manage AWS infrastructure.
	
	- Monitoring and alerting failure
	
	We are monitoring several new aspects of the runner system:
	Taskscaler, AWS instances, VPN bridge. As much as possible the new
	architecture handles monitoring and alerting in the same way as
	the existing runners. However there is still a chance we could
	miss a problem in a new system.

- [ ] **What are a few operational concerns that will not be present
      at launch, but may be a concern later?**

	- AWS Mac Quota and Capacity
	
	At launch we will not have a lot of customers using mac
	runners. However as this grows organically we may need to request
	additional capacity. And we may run into physical capacity limits.
	
	We have already forecasted our capacity requirements:
	https://docs.google.com/spreadsheets/d/1u-lmwgGtD9F_5bMUuyGsvLg00_pIhVg14uktX8FPQ0I/edit#gid=446804411
	(restricted) and communicated them to AWS. They will have
    sufficient capacity set aside for our production account.

	- Apple Changing License Restrictions
	
	A great deal of the technical complexity of the mac runner
	architecture comes from the limitations placed on mac instances by
	Apple. E.g. 24-minimum allotment, max 2 VMs per instance. If Apple
	changes their licensing then we have little choice but to comply,
	which could invalidate these new aspect of our architecture.

- [ ] **Can the new product feature be safely rolled back once it is
      live, can it be disabled using a feature flag?**

    The new feature is a new Runner that we will register and make
    available as _instance_ Runner on GitLab.com.  Should it be
    needed, we can always pause the runner in GitLab UI or stop the
    runner process in MacStadium. The only downside is that jobs
    targeting explicitly this runner will become stuck.

- [ ] **Document every way the customer will interact with this new
      feature and how customers will be impacted by a failure of each
      interaction.**

    Customers will create GitLab CI/CD jobs that will be targeting the
    macOS Runners. If - for any reason - the runner will be
    unavailable or will not work properly, the users will either get
    their jobs stuck in `pending` state or will get unexpected job
    failures and results.

- [ ] **As a thought experiment, think of worst-case failure scenarios
      for this product feature, how can the blast-radius of the
      failure be isolated?**

    The blast radius is already isolated. In worst case scenario, jobs
    targeting macOS SaaS Runners will be unable to execute. Any other
    jobs or system part should not be affected.

## Database

N/A

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes,
  please list them here or link to a place where they are listed)**

  - [ ] **AWS Accounts/GCP Projects**

	AWS sandbox account (for development): 915502504722
	AWS staging account (for image building): 251165465090
	AWS prod account TBD: https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/21

  - [ ] **New Subnets**
  
    AWS/r-saas-m-staging
	jobs-vpc/saas-macos-staging-blue-1
	10.20.0.0/21

	AWS/r-saas-m-staging
	jobs-vpc/saas-macos-staging-blue-2
	10.20.8.0/21

	AWS/r-saas-m-staging
	jobs-vpc/saas-macos-staging-green-1
	10.20.16.0/21

	AWS/r-saas-m-staging
	jobs-vpc/saas-macos-staging-green-2
	10.20.24.0/21

	AWS/r-saas-m-m1
	jobs-vpc/saas-macos-m1-blue-1
	10.30.0.0/21

	AWS/r-saas-m-m1
	jobs-vpc/saas-macos-m1-blue-2
	10.30.8.0/21

	AWS/r-saas-m-m1
	jobs-vpc/saas-macos-m1-green-1
	10.30.16.0/21

	AWS/r-saas-m-m1
	jobs-vpc/saas-macos-m1-green-2
	10.30.24.0/21

	(Source: https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/ci-runners#network-info)

  - [ ] **VPC/Network Peering**

	There is a GCP-AWS peering for both staging and production.
	
	Here is the current configuration for staging:
	https://gitlab.com/gitlab-com/gl-infra/config-mgmt/-/blob/235afc1778b1bbabc91818933d367ecf8ca911ec/modules/ci-runners/fleeting-aws-env/gcp-to-aws-vpn.tf
	
	Production is currently being setup:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/22

  - [ ] **DNS names**

	N/A

  - [ ] **Entry-points exposed to the internet (Public IPs,
        Load-Balancers, Buckets, etc...)**

	The AWS subnets are configured to provide public IPs to mac
	instances because they need to talk to gitlab.com to pull down the
	job definition and code. The AWS VPC is configured with a policy
	that allows all outgoing traffic but only incoming traffic on port
	22 (ssh).

	We can remove public access by setting up a bastion server in the
    `gitlab-ci-155816` project which already has VPN access to the
    machines. But this is left for later.
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/64

  - [ ] **Other (anything relevant that might be worth mention)**

	N/A

- **Secure Software Development Life Cycle (SSDLC)**
    - [ ] **Is the configuration following a security standard? (CIS
          is a good baseline for example)**

	These are the relevant controls from CIS Apple macOS 12.0 Monterey
    Benchmark (v2.0.0 - 11-21-2022)
	
		- 1.1. "Software updates should be run at minimum every 30 days."
          So we will need to build and release both infrastructure and
          job images with security updates at least every 30 days

		- 2.4.5 "Ensure Remote Login is Disabled". We do not follow
          this recommendation by design, because all access to these
          Macs is remote since we are using them as servers.

		- Various controls around login screen, backup etc.. are not
          relevant because we disable these features altogether.

		- Password controls are not relevant because infrastructures
          images are accessed only via SSH key and worker images are
          accessed by a known password only from the infrastructure
          images (username: `gitlab`, password: `gitlab`).

	See also system preparation scripts for the infrastructure images:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/macos-nesting/-/tree/master/scripts
	and the job image preparation instructions:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/blob/main/README.md#create-a-new-base-os-image

	We can also programatically verify the relevant CIS controls:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/67

    - [ ] **All cloud infrastructure resources are labeled according
          to the [Infrastructure Labels and
          Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/)
          guidelines**

	Yes, all resources are labeled accordingly:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/issues/111

    - [ ] **Were the [GitLab security development
          guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html)
          followed for this feature?**

          N/A

          This feature is not a GitLab application change. It adds new
          GitLab Runner environment to use by customers.

    - [ ] **Do we have an automatic procedure to update the
          infrastructure (OS, container images, packages, etc...)**

	We have an automated process for building and releasing the infrastructure images:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/37
	
	The job images are still built manually but automation will follow shortly:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/50

    - [ ] **Do we use IaC (Terraform) for all the infrastructure
          related to this feature? If not, what kind of resources are
          not covered?**

	Yes.

        - [ ] **Do we have secure static code analysis tools
              ([kics](https://github.com/Checkmarx/kics) or
              [checkov](https://github.com/bridgecrewio/checkov))
              covering this feature's terraform?**

		We are using `tflint` on our Terraform templates.

  - **If there's a new terraform state:**
    - [ ] **Where is to terraform state stored, and who has access to it?**

	Terraform state is stored in S3 with limited access only for SREs,
    just like the Linux runners.

  - [ ] **Does this feature add secrets to the terraform state? If
        yes, can they be stored in a secrets manager?**

	No.

  - **If we're creating new containers:**

	No, we are not create new containers.

    - [ ] **Are we using a distroless base image?**
	
	N/A

    - **Do we have security scanners covering these containers?**
      - [ ] **`kics` or `checkov` for Dockerfiles for example**

	N/A	

      - [ ] **[GitLab's
            container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration)
            scanner for vulnerabilities**

	N/A

- **Identity and Access Management**
  -  [ ] Are we adding any new forms of **Authentication** (New
         service-accounts, users/password for storage, OIDC, etc...)?

	We are creating a new AWS service account capable of
	controlling the ASG. This will be used by runner manager in GCP to
	scale the ASG in AWS.

  -  [ ] **Does it follow the least privilege principle?**
  
	Yes. This is the policy used:
    https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws/-/blob/76ee93f27038c4b70e733af232f67a70eb65078e/README.md#recommended-iam-policy

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  -  [ ] **What kind of data is stored on each system? (secrets,
         customer data, audit, etc...)**

	We are adding an AWS S3 cache bucket. This cache will store
	arbitrary user job data.

  -  [ ] **How is data rated according to our [data classification
         standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
         (customer data is RED)**

	Red because it is customer data.

  -  [ ] **Is data it encrypted at rest? (If the storage is provided
         by a GCP service, the answer is most likely yes)**

    All user data is stored on the VM that executes the job. The VMs
    are configured to be removed after one usage.  No user data should
    be persisted after job execution is finished.

    As for our other runners, we're using a remote cache
    feature. Which means that some data that users decide to cache
    across jobs will be copied to a dedicated S3 bucket. The bucket is
    not used for anything else, and access to it is restricted only to
    the runners and infrastructure maintainers (SaaS Runners engineers
    and SREs).

  -  [ ] **Do we have audit logs on data access?**

	TODO: add audit logging on S3 bucket

	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/issues/112

- **Network security (encryption and ports should be clear in the
  architecture diagram above)**
  -  [ ] **Firewalls follow the least privilege principle (w/ network
         policies in Kubernetes or firewalls on cloud provider)**

	AWS firewalls are called security groups. They allow all outgoing
	traffic and incoming traffic on ports 22 and 8 (ssh and
	ICMP). This is configured in the AWS environment Terraform
	template.
	
	This will be restricted to the internal IP address of the bastion
    server:
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/64

  -  [ ] **Is the service covered by any DDoS protection solution
         (GCP/AWS load-balancers or Cloudflare usually cover this)**

	The only traffic that goes to gitlab.com from outside comes from
	the mac runners. This goes through Cloudflare which provides DDoS
	protection.

  -  [ ] **Is the service covered by a WAF (Web Application
         Firewall)**

	N/A (not a web application)

- **Logging & Audit**
  - [ ] **Has effort been made to obscure or elide sensitive customer
        data in logging?**

    CI jobs are logging customer specific data only to the job traces,
    which are stored in the temporary files on the Runner host
    (removed after job is finished) and are pushed back to
    GitLab. Additionally, variables set by customers to be masked, are
    masked before they are stored in the buffer file.

- **Compliance**
    - [ ] **Is the service subject to any regulatory/compliance
          standards? If so, detail which and provide details on
          applicable controls, management processes, additional
          monitoring, and mitigating factors.**

	No.

## Performance

- [ ] **Explain what validation was done following GitLab's
      [performance
      guidelines](https://docs.gitlab.com/ee/development/performance.html). Please
      explain which tools were used and link to the results below.**

	N/A

	This feature uses the existing GitLab Runner API and doesn't add
    any new queries.

- [ ] **Are there any potential performance impacts on the database
      when this feature is enabled at GitLab.com scale?**

    N/A

- [ ] **Are there any throttling limits imposed by this feature? If so
      how are they managed?**

    When we hit the capacity limits, no new jobs will be executed
    until some capacity is freed. This means that customers may start
    seeing their jobs targeting macOS SaaS Runners to be stuck in the
    `pending` state for longer than expected.

- [ ] **If there are throttling limits, what is the customer
      experience of hitting a limit?**

    We will be using a pre-determined amount of nodes building our
    clusters where jobs are executed.

    Within these clusters, we will be managing the capacity by
    configuring Runner's `concurrent` and `limit` settings. These are
    managed by the Terraform templates, and are applied through CI
    jobs [in a dedicated management
    project](https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos).

- [ ] **For all dependencies external and internal to the application,
      are there retry and back-off strategies for them?**

	TODO: verify back-off for all dependencies

	https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/issues/113

- [ ] **Does the feature account for brief spikes in traffic, at least
      2x above the expected TPS?**

    Capacity works the same as Linux runners. If 2x traffic arrives it
    will cause a scale up and more runners will come online.

## Backup and Restore

- [ ] **Outside of existing backups, are there any other customer data
      that needs to be backed up for this product feature?**

	No other customer data needs to be backed up. The cache bucket is
	ephermeral and cache misses do not fail a job. All persistent
	customer data is stored in existing GitLab systems.

- [ ] **Are backups monitored?**

	N/A

- [ ] **Was a restore from backup tested?**

	N/A

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded
      to logstash?**

	Logs are forwarded to GCP Cloud Logging which sends logs to PubSub
	the same as the Linux runners.

- [ ] **Is the service reporting metrics to Prometheus?**

	Yes, Prometheus in the `gprd` network scrapes the mac runner
	managers in the `ci` network.

	We will setup monitoring for AWS workers and the GCP-AWS VPN.
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/65
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/66
	
	We are also setting up exposing a queue duration histogram metric:
	https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/3499

- [ ] **How is the end-to-end customer experience measured?**

    Same measurements as for other SaaS Runners.

    As the feature brings a new SaaS Runners environment to the pool
    and a lot of things that will happen there will depend on whether
    the CI/CD jobs executed by users are valid by themselves, there
    aren't many general measurements that we could look on. One of the
    ways how we think about end-to-end experience of the SaaS Runner
    user is that **an average job should complete when initiated by
    the user; a Runner infrastructure failure should not be the reason
    that a job didn't finish**.

- [ ] **Do we have a target SLA in place for this service?**

    No SLA defined for now. MacOS SaaS Runners will be counted in to
    `ci-runners` service apdex. We will be working on improving GitLab
    CI/CD and GitLab Runner metrics to make it possible to define
    individual SLA for each SaaS Runners shard.

    For now all platforms that we host - Linux, Windows and macOS -
    are tracked by the same apdex and the SLA is based on top of that.

    However, given the difference between number of jobs executed on
    Linux (huge) and number of jobs executed on Windows or macOS (way,
    way lower), macOS SaaS Runners will be mostly negligible. We will
    be working on making it possible to define different SLAs for
    different shards that we operate.

- [ ] **Do we know what the indicators (SLI) are that map to the
      target SLA?**

    Same as for SLA

- [ ] **Do we have alerts that are triggered when the SLI's (and thus
      the SLA) are not met?**

    Yes.
	
	Mac runner availability is covered by the same SLI's as the Linux
    runners:
	https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#verify-verifyrunner---error-budget-for-gitlabcom

- [ ] **Do we have troubleshooting runbooks linked to these alerts?**

    Yes. We will need to update the runbooks to link to macOS SaaS
    Runners specific content when the alert is related to
    `macos-shared` shard.

- [ ] **What are the thresholds for tweeting or issuing an official
      customer notification for an outage related to this feature?**

    Same as for the existing Linux SaaS runners.

- [ ] **do the oncall rotations responsible for this service have
      access to this service?**

	Yes.

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the
      most about this feature?**

    | Role | DRI |
    |------|-----|
    | PM   | [Darren Eastman](https://gitlab.com/DarrenEastman)  |
    | EM   | [Elliot Rushton](https://gitlab.com/erushton)       |
    | BE   | [Tomasz Maczukin](https://gitlab.com/tmaczukin)     |
    | BE   | [Joseph Burnett](https://gitlab.com/josephburnett)  |
    | BE   | [Davis Bickford](https://gitlab.com/dbickford)      |

- [ ] **Which team or set of individuals will take responsibility for
      the reliability of the feature once it is in production?**

    GitLab Runner group ([`#g_runner` on
    Slack](https://gitlab.slack.com/archives/CBQ76ND6W)).

    We will however not be on call for this. If, for example, things
    break at midday (North America times) on a Friday, then likely
    they won't get resolved until our European engineers come online
    the following Monday. The runbooks can be used by the SREs but
    there's no illusion that they replace familiarity with the
    platform.

- [ ] **Is someone from the team who built the feature on call for the
      launch? If not, why not?**

    The whole team behind macOS SaaS Runners project is driving it, so
    most of us should be available when doing the launch.

## Testing

- [ ] **Describe the load test plan used for this feature. What
      breaking points were validated?**

	https://gitlab.com/gitlab-org/gitlab/-/issues/383018

- [ ] **For the component failures that were theorized for this
      feature, were they tested? If so include the results of these
      failure tests.**

	TODO: manual failure testing
	
	https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/41

- [ ] **Give a brief overview of what tests are run automatically in
      GitLab's CI/CD pipeline for this feature?**
  
  Unit tests are automatically run on all new components:
  https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29318
  
  We also run a regular end-to-end test of the new autoscaler with the
  AWS plugin:
  https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws/-/issues/4
