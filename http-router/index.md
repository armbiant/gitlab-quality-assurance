# HTTP Router

The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Experiment

**For clarification, Experiment covers the rollout to 100% of requests to pre.gitlab.com.**

### Service Catalog

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here:
  - <https://gitlab.com/gitlab-com/runbooks/-/blob/532b154d6c455fafe32e3cc92fe960f75a9f2f74/services/service-catalog.yml#L714>
  - Link to or provide a high-level summary of this new product feature: <https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/http_routing_service>
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.  
    - <https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/#architecture>
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.
  - List individuals are the subject matter experts and know the most about this feature.
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.
  - List the member(s) of the team who built the feature will be on-call for the launch.
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.
    - Cloudflare Workers: HTTP Router runs on [cloudflare workers](https://workers.cloudflare.com/), if there is a problem with Cloudflare workers or Cloudflare we won't be able to route any request to GitLab.

### Infrastructure

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
  - Yes, we use [`cloudflare workers routes`](https://developers.cloudflare.com/workers/configuration/routing/routes/), managed through [config-mgmt](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/420a24b2a432c82c40d3e3265b99057ab5b45540/environments/pre/cloudflare-workers.tf)
- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?
  - Yes, this is part of Cloudflare
- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?
  - All resources are provisioned within Cloudflare's ecosystem. Due to Cloudflare's current limitations, we cannot apply labels to these resources as their platform does not support object labeling.

### Operational Risk

- [x] List the top three operational risks when this feature goes live.
  - Increased latency, see <https://docs.gitlab.com/ee/architecture/blueprints/cells/routing-service.html#low-latency>
  - Increased cost, see <https://gitlab.com/gitlab-org/gitlab/-/issues/433471#note_1707800071>
  - The service operates on [`Cloudflare Workers`](https://developers.cloudflare.com/workers/), a managed Platform-as-a-Service (PaaS) solution. As with any external service dependency, our availability is directly tied to Cloudflare Workers' operational status.
- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?
  - Cloudflare Worker platform: Downtime on Cloudflare Worker will cause all requests to fail.
  - HTTP-Router: A bug in the HTTP Router will cause full or partial failure of routing requests to GitLab.
  - Given the HTTP router's critical position at the edge of our networking stack, options for reducing the blast radius of potential failures are limited. Any disruption to the router's functionality could potentially impact our entire infrastructure, leading to widespread service interruptions.

### Monitoring and Alerting

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service
  - The metrics catalog entry is located [here](https://gitlab.com/gitlab-com/runbooks/-/blob/e1ea88dea9ce7559583344c06ae4949e50db133b/metrics-catalog/services/http-router.jsonnet).

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - No, the experiment will be done on `pre.gitlab.net` so we'll learn how to rollout the router part of this deployment.
- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?
  - Initially yes, the route/worker can be disabled and traffic will flow directly to Origin as before
  - As we introduce more cells, this service becomes critical for routing requests to the correct cell. Without it, users would be unable to access GitLab.com.
- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline)
  - [Cloudflare Worker Router](https://developers.cloudflare.com/workers/configuration/routing/routes/) is managed through `terraform` inside [`config-mgmt`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/08af68273fb65336cb49ec60c647816bb0e52940/environments/pre/cloudflare-workers.tf).
  - Worker code deployments are managed through GitLab CI/CD pipelines, as detailed in our [deployment documentation](https://gitlab.com/gitlab-org/cells/http-router/-/blob/c0bbfaae75be7d534713564aa29866af78705dd1/docs/deployment.md).

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: N/A
  - New Subnets: N/A
  - VPC/Network Peering: N/A
  - DNS names: Disable `workers.dev` domain: <https://gitlab.com/gitlab-org/cells/http-router/-/blob/a097e6d0c73bd33b5dd0770134359cd05aa3623e/wrangler.toml#L5-6>
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
    - `pre.gitlab.com`
    - `staging.gitlab.com`
    - `gitlab.com`
  - Other (anything relevant that might be worth mention): N/A
- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
  - Yes, we run the [SAST Scanner](https://docs.gitlab.com/ee/user/application_security/sast/) for [every merge request](https://gitlab.com/gitlab-org/cells/http-router/-/blob/34cdf83a5afda7d3dca686d73315de7c13202c41/.gitlab-ci.yml#L6-L8) to ensure we don't introduce any vulnerabilities.
- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
  - Yes, Link to threat model <https://gitlab.com/gitlab-com/gl-security/product-security/appsec/threat-models/-/blob/master/gitlab-org/cells/README.md?ref_type=heads> also the issue is created on the AppSec board <https://gitlab.com/gitlab-org/cells/http-router/-/issues/7>
- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
  - Yes, we have renovate bot configured for both, [http-router](https://gitlab.com/gitlab-org/cells/http-router/-/blob/480170957188fab2eaf29c37c83bba969dd5348b/renovate.json) and [http-router-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer/-/blob/6aeee58281e6bb10ee3153bc7076b9d0ad5fa4d7/renovate.json)
- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.
  - Yes
- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?
  - Yes, we are using [ci-images](https://gitlab.com/gitlab-com/gl-infra/ci-images/-/tree/master/wrangler?ref_type=heads) to manage our wrangler image. Which have the necessary scanners [configured](https://gitlab.com/gitlab-com/gl-infra/ci-images/-/blob/ae7face2326d983d4345c06109d1712d9579df99/.gitlab-ci.yml#L3).

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?
  - No
- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?
  - The service will forward requests to the correct Cell depending on the secrets and paths of the requests. The blueprint has more details on how the router allows the traffic to go to the correct Cell ([ref](https://gitlab.com/gitlab-org/gitlab/-/blob/78f545339a08448019f00e3f7212ab440a50bd38/doc/architecture/blueprints/cells/routing-service.md#router-configured-to-perform-static-routing)).
- [~] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
  - N/A, HTTP router is a SaaS.
- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?
  - Yes, GitLab.com is behind a Cloudflare WAF. We will place HTTP Router behind this WAF.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?
  - Cloudflare [Worker Logs](https://developers.cloudflare.com/workers/observability/logs/workers-logs/) automatically redacts tokens
  - [Exception Logs](https://gitlab.com/gitlab-org/cells/http-router/-/issues/94) will not be redacted, we will route on things like Personal Access Tokens which would be required for debugging purposes.
- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
  - Cloudflare Worker stores basic information:
    - HTTP request information like URL, headers if Real-time logs is
      [enabled](https://developers.cloudflare.com/workers/observability/logging/).
    - Wall time per execution, https://developers.cloudflare.com/workers/observability/metrics-and-analytics/.
- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?
  - RED. The data passed through the router is the same as Cloudflare WAF, it is possible to decrypt HTTP payloads.
- [x] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
  - Cloudflare logs are already being ingested by our SIEM
- [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
  - Our logging strategy combines [Cloudflare Worker Logs](https://developers.cloudflare.com/workers/observability/logs/workers-logs/) for all logs with a 1% head-based sampling and [Sentry SDK for Cloudflare](https://www.npmjs.com/package/@sentry/cloudflare) for handling all the exceptions.:
    - [Cloudflare Worker Logs](https://developers.cloudflare.com/workers/observability/logs/workers-logs/) provide a 7-day retention period with 1% head-based sampling. For more details, refer to the [Worker Logs Overview](https://gitlab.com/gitlab-com/runbooks/-/blob/288caa9c382b6e85606be245505b0cf2685e5306/docs/http-router/logging.md).
    - [Sentry](https://www.npmjs.com/package/@sentry/cloudflare) captures all worker exceptions. For more details, see the [Worker Sentry Integration](https://gitlab.com/gitlab-com/runbooks/-/blob/288caa9c382b6e85606be245505b0cf2685e5306/docs/http-router/logging.md).
- [x] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.
  - <https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/45>

## Beta

**For clarification, Beta covers the rollout to 100% of requests to staging.gitlab.com.**

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to examples of logs on https://logs.gitlab.net
  - Our logging strategy combines [Cloudflare Worker Logs](https://developers.cloudflare.com/workers/observability/logs/workers-logs/) and [Cloudflare Sentry Integration](https://www.npmjs.com/package/@sentry/cloudflare):
    - [Cloudflare Worker Logs](https://developers.cloudflare.com/workers/observability/logs/workers-logs/) provide a 7-day retention period with 1% head-based sampling. These logs are present cloudflare dashboard itself: <https://dash.cloudflare.com/852e9d53d0f8adbd9205389356f2303d/workers/services/view/production-gitlab-com-cells-http-router/production/observability/logs>. For more details, refer to the [Worker Logs Overview](https://gitlab.com/gitlab-com/runbooks/-/blob/288caa9c382b6e85606be245505b0cf2685e5306/docs/http-router/logging.md).
    - [Cloudflare Worker Sentry Integration](https://www.npmjs.com/package/@sentry/cloudflare) captures all worker exceptions. For more details, see the [Worker Sentry Exception](https://gitlab.com/gitlab-com/runbooks/-/blob/288caa9c382b6e85606be245505b0cf2685e5306/docs/http-router/logging.md#worker-sentry-integration).
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.
  - <https://dashboards.gitlab.net/d/http-router-main/http-router3a-overview?orgId=1>

### Backup, Restore, DR and Retention

The HTTP Router is deployed globally across Cloudflare's Points of Presence (PoPs), enabling low-latency responses for users worldwide. This distributed architecture ensures high availability and optimal performance across different geographical regions.

We deliberately opted not to use [smart-placement](https://developers.cloudflare.com/workers/configuration/smart-placement/) and deliberately stuck with the `Default Placement` for our Worker deployment. While smart-placement optimizes Worker location relative to origin servers, our use case requires positioning Workers closer to end users to minimize latency and improve response times. This is precisely what Default Placement does, as it deploys Workers to Cloudflare's global network, ensuring they are positioned closer to end users by default.

As the HTTP Router is a stateless service, it does not require any backup or restore procedures.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - Initial traffic routing to `staging.gitlab.com` was implemented through [Infrastructure Change Management #18482](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18482).
  - Worker code deployments are managed through GitLab CI/CD pipelines, as detailed in our [deployment documentation](#deployment), rather than through the standard [change management process](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/).
- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
  - No, this is outside of our deploymenet process and can be deployed indepently of any other service.
- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
  - We run the [QA smoke tests](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer/-/blob/6aeee58281e6bb10ee3153bc7076b9d0ad5fa4d7/.gitlab-ci.yml#L116) after the deployment.
- [x] Will it be possible to roll back this feature? If so explain how it will be possible.
  - It is possible to rollback the worker deployment using the manual `rollback` job in the deploy pipline as described in our [Rollback docs](https://gitlab.com/gitlab-org/cells/http-router/-/blob/34cdf83a5afda7d3dca686d73315de7c13202c41/docs/deployment.md#rollback).
  - In case a rollback is not possible using GitLab CI (e.g., due to a Vault outage preventing the job from running), it is always possible to perform a rollback directly from the Cloudflare Workers dashboard.

### Security

_The items below will be reviewed by the InfraSec team._

- [x] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
  - The HTTP Router utilizes session cookies for cell-based routing. While an attacker could potentially attempt to manipulate routing by forging session cookies, this security risk is mitigated by cell-specific session validation. Any requests with invalid or forged session cookies will result in a 403 Forbidden response, ensuring unauthorized access attempts are blocked.
  - The HTTP Router's security architecture leverages Cloudflare Workers' robust isolation model, which utilizes V8 isolates to ensure secure runtime environments. This implementation, combined with Cloudflare's Web Application Firewall (WAF), provides comprehensive protection against potential security threats. For detailed information, refer to the [Cloudflare Workers Security Model](https://developers.cloudflare.com/workers/reference/security-model/).
- [x] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?
  - Not applicable, there are no outstanding issues.

## General Availability

**For clarification, General Availability covers the rollout to 100% of requests for gitlab.com**

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to the troubleshooting runbooks.
  - [Runbook Docs](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/http-router?ref_type=heads#troubleshooting-pointers)
  - [Deployment Docs](https://gitlab.com/gitlab-org/cells/http-router/-/blob/34cdf83a5afda7d3dca686d73315de7c13202c41/docs/deployment.md).
  - It is possible to rollback the worker deployment using the manual `rollback` job in the deploy pipline, as described in the [Rollback Docs](https://gitlab.com/gitlab-org/cells/http-router/-/blob/34cdf83a5afda7d3dca686d73315de7c13202c41/docs/deployment.md#rollback).
- [x] Link to an example of an alert and a corresponding runbook.
  - We'll get alerts from Pingdom and Blackbox in case the service isn't working properly.
  - Additional alerts will be added as part of: <https://gitlab.com/gitlab-org/cells/http-router/-/issues/98>
- [x] Confirm that on-call Reliability SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.
  - Since the service is managed through Cloudflare which every SRE already have access to.

### Operational Risk

- [x] Link to notes or testing results for assessing the outcome of failures of individual components.
  - Performance analysis demonstrates minimal latency impact from Worker implementation. For detailed metrics, refer to our [latency assessment report](https://gitlab.com/gitlab-org/cells/http-router/-/issues/21#note_2128511828).
  - Internal route performance metrics in production environments are documented in our [routing analysis](https://gitlab.com/gitlab-org/cells/http-router/-/issues/19#note_2173382330).
  - As Cloudflare Workers operate at the edge of our network infrastructure, any Cloudflare service disruption would affect our service availability. This represents a critical dependency in our architecture.
  - Since the CI relies on [Vault](https://www.vaultproject.io/), any disruption to it will prevent us from rolling out a new version.
- [x] What are the potential scalability or performance issues that may result with this change?
  - Cloudflare workers are a SaaS offering which is highly sclable with traffic.
    - Ref: <https://developers.cloudflare.com/learning-paths/workers/concepts/workers-concepts/> and <https://developers.cloudflare.com/learning-paths/workers/concepts/serverless-computing/>
  - We would have limits to our traffic as described in the [worker docs](https://developers.cloudflare.com/workers/platform/limits/).
- [x] What are a few operational concerns that will not be present at launch, but may be a concern later?
  - When we introduced more [routing rules](https://gitlab.com/gitlab-org/cells/http-router/-/blob/7ec4201e48040d0b52c47e86136c20b2bf6d5a80/docs/rules.md) in the router, apart from the passthrough rule that we are launching with.
  - Service availability is dependent on the Topology Service. Any disruption to the Topology Service may impact request routing functionality and overall system performance. We try to minimize the impact by:
    - Having a multi-regional `Topology Service` (us-east1 and us-central1) which we load balance between the two.
    - Using [worker cache](https://developers.cloudflare.com/workers/runtime-apis/cache/) to cache the topology service responses.
- [x] Are there any single points of failure in the design? If so list them here.
  - Cloudflare is already a single point of failure since all traffic is routed through it. There is a higher probability of hitting issues since we are using more services from Cloudflare.
- [x] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?
  - If the cloudflare workers are having an outage and as a result, we are not able to proxy any request, we can remove the worker route, so they can be bypassed to the legacy cell. However, it won't work in the future when we start routing requests to Cells.

### Backup, Restore, DR and Retention

The HTTP Router is deployed globally across Cloudflare's Points of Presence (PoPs), enabling low-latency responses for users worldwide. This distributed architecture ensures high availability and optimal performance across different geographical regions.

We deliberately opted not to use [smart-placement](https://developers.cloudflare.com/workers/configuration/smart-placement/) and deliberately stuck with the `Default Placement` for our Worker deployment. While smart-placement optimizes Worker location relative to origin servers, our use case requires positioning Workers closer to end users to minimize latency and improve response times. This is precisely what Default Placement does, as it deploys Workers to Cloudflare's global network, ensuring they are positioned closer to end users by default.

As the HTTP Router is a stateless service, it does not require any backup or restore procedures.

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [x] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
  - Cloudflare Workers operate on a globally distributed network spanning over 300 cities across 120+ countries. This architecture ensures automatic scalability to handle traffic fluctuations, maintaining consistent application performance. For detailed information, refer to Cloudflare's documentation on [Worker concepts](https://developers.cloudflare.com/learning-paths/workers/concepts/workers-concepts/) and [serverless computing](https://developers.cloudflare.com/learning-paths/workers/concepts/serverless-computing/).
  - Cloudflare Workers have been successfully implemented in other GitLab services, such as `about.gitlab.com`, demonstrating proven scalability within our infrastructure.
  - We did performance validation as part of: <https://gitlab.com/gitlab-org/cells/http-router/-/issues/19> and <https://gitlab.com/gitlab-org/cells/http-router/-/issues/21>.
- [x] Link to any load testing plans and results.
  - Cloudflare Workers have demonstrated robust scalability in various GitLab services, including `about.gitlab.com`. This proven track record in our existing infrastructure provides confidence in the platform's ability to handle our scaling requirements.
- [x] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
  - The HTTP Router operates at the network edge, positioned behind Cloudflare's Web Application Firewall (WAF) and in front of GitLab.com's core infrastructure. This placement eliminates direct interaction with any database or Redis instances.
  - For routing decisions, the HTTP Router interfaces with the Topology Service, which maintains its own dedicated database for cell metadata management.
- [x] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
  - No, we do not apply the rate-limiting on this layer, It sits between WAF and our Rate-limiting stack.
- [x] Are there retry and back-off strategies for external dependencies?
  - It does not call any external dependency right now, but it will once we introduce Topology Service and start [routing to other cells](https://gitlab.com/gitlab-org/cells/http-router/-/blob/7ec4201e48040d0b52c47e86136c20b2bf6d5a80/docs/rules.md). Currently, we don't have any retry and back-off strategy, they'll be added as part of: <https://gitlab.com/gitlab-org/cells/http-router/-/issues/101>
- [x] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?
  - Yes, we are leveraging Cloudflare workers which can scale very easily. For detailed information, refer to Cloudflare's documentation on [Worker concepts](https://developers.cloudflare.com/learning-paths/workers/concepts/workers-concepts/) and [serverless computing](https://developers.cloudflare.com/learning-paths/workers/concepts/serverless-computing/).

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - The Cloudflare worker route will be added as part of the CR: <https://ops.gitlab.net/gitlab-com/gl-infra/production/-/issues/10>.
  - The code changes to the worker are deployed through GitLab CI as described in our [Deployment docs](https://gitlab.com/gitlab-org/cells/http-router/-/blob/34cdf83a5afda7d3dca686d73315de7c13202c41/docs/deployment.md).
- [x] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
  - Failing Pingdom/Blackbox checks.
  - Increased latency for `GitLab.com` and http-router specific alert.
  - QA test starts failing due to connectivity issues after a deployment.
  - Reduced traffic coming to our Web and API components.
- [x] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
  - No, all of the [deployment](https://gitlab.com/gitlab-org/cells/http-router/-/blob/c0bbfaae75be7d534713564aa29866af78705dd1/docs/deployment.md) is driven through the `ops.gitlab.net`.
