# Packagecloud

Epic: <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/922>

[[_TOC_]]

## Introduction

[Packagecloud](https://packagecloud.io) is the application that powers
[packages.gitlab.com](https://packages.gitlab.com), which is where the GitLab package repositories live that our users
interact with to install & upgrade the RPM & DEB packages provided by GitLab.

The service was deployed many years ago (circa 2015) with all components (MySQL, Redis and all Packagecloud components)
running on a single VM (SPoF). Over time, we've had a [number of
incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=created_date&state=all&label_name%5B%5D=incident&label_name%5B%5D=Service%3A%3APackageCloud&first_page_size=20)
and we've tried a number of things to keep the service running reliably including upsizing the instance,
migrating components from running on the VM to managed services (RDS, Elasticache), etc. But even with these changes, we
would still run into resource contention issues during package publishing, which affected the delivery team resulting in release delays (i.e., having to retry jobs). It was particularly impactful during
time-sensitive releases like security releases.

With [Reliability:General](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/general.html)
taking ownership for the packages.gitlab.com service, the service gained more focus & attention. The team decided that
the service needed to be able to scale under load and self-heal so a
[proposal](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24245) was filed and ultimately implemented to
migrate as many components as possible from AWS to Kubernetes (GKE).

## Architecture

See [architecture
overview](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/packagecloud/infrastructure.md#architecture-overview).

See the [service catalog entry
(permalink)](https://gitlab.com/gitlab-com/runbooks/-/blob/39c6b08e03d3a13cf4a4231207dcd69d031f24cb/services/service-catalog.yml#L2207-2232)
for the service.

### Dependencies

The service depends on the following services:

- Cloudflare (CDN)
- Memorystore (Redis) for package uploads (rainbows) & indexing (resque)
- CloudFront & S3 for package uploads (rainbows), indexing (resque) and users downloading packages (unicorn)
- Cloud SQL (MySQL) for _everything_

## Contacts

[Reliability:General](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/general.html) are the service owners.

[Gonzalo Servat](https://gitlab.com/gsgl) (SRE in Reliability:General) wrote the [proposal](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24245), the [Docker
image](https://ops.gitlab.net/gitlab-com/gl-infra/ci-images/-/tree/master/packagecloud), the [Helm
Chart](https://gitlab.com/gitlab-com/gl-infra/charts/-/tree/main/gitlab/packagecloud) and migrated the service in
close collaboration with the
[Reliability:Foundations](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/foundations.html)
team.

## Infrastructure

_The items below will be reviewed by the Reliability team._

✅ Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources
  are not covered?

All Infrastructure **except** for CloudFront (managed by the application) is defined in [Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/ops/packagecloud.tf).

✅ Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

Traffic is proxied by Cloudflare (packages.gitlab.com) to the Google Cloud Load Balancer.

❌ Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

No ([issue filed](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24561)).

## Operational Risk

_The items below will be reviewed by the Reliability team._

✅ List the top three operational risks when this feature goes live.

The packages.gitlab.com service has been live for many years.

✅ For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

We have multiple Packagecloud deployments that are responsible for different functions of the service:

- `web`: any requests that do not start with `/api/` are served by this deployment (e.g., user authentication, package downloads)
- `rainbows`: any requests that start with `/api/` are served by this deployment (i.e., mainly package uploads but any
  other API requests would also land here)
- `resque`: processes package indexing requests once they've been uploaded via `rainbows`

The above deployments depend on the following external dependencies:

- Failure of Cloud SQL (MySQL) would affect most/all functionality of packages.gitlab.com (package downloads, uploads,
indexing).
- Failure of Memorycache (Redis) would affect package uploads & indexing operations only.
- Failure of CloudFront and/or S3 would affect package downloads.

## Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

✅ Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

<https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/packagecloud.jsonnet>

## Deployment

_The items below will be reviewed by the Delivery team._

✅ Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be
used for rollout? If so, link to it here.

No CR for the initial packages.gitlab.com deployment, however
[#16271](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/16271) is the CR for migrating the service from AWS
to GKE.

✅ Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

N/A (has been live for years)

✅ How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

Docker image is being built inside our `ci-images` repository: <https://ops.gitlab.net/gitlab-com/gl-infra/ci-images/-/tree/master/packagecloud>

## Security Considerations

_The items below will be reviewed by the Infrasec team._

✅ Link or list information for new resources of the following type:

- AWS Accounts/GCP Projects:
  - AWS: `855262394183` (`GitLab SaaS - Production`)
  - GCP: `gitlab-ops` (existing project)
- New Subnets:
- VPC/Network Peering:
- DNS names:
  - `packages.gitlab.com`
- Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Public IP
  - GCP Load Balancer
  - AWS S3 Bucket
- Other (anything relevant that might be worth mention):
  - CloudFront Distribution managed by Packagecloud

✅ Were the [GitLab security development
  guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

N/A - Packagecloud is a third-party commercial package.

✅ Was an [Application Security
  Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/)
  requested, if appropriate? Link it here.

<https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/221>

✅ Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For
  example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies
  up-to-date?

Updating the version of Packagecloud in our Docker image is managed
[manually](https://ops.gitlab.net/gitlab-com/gl-infra/ci-images/-/blob/b006e29e7849218201af1b4c60595e1e18e6be69/packagecloud/Dockerfile#L26)
as the Packagecloud Debian package lives in a private repository.
Once a new Docker image is built, Renovate Bot is used to bump the
[Packagecloud chart](https://gitlab.com/gitlab-com/gl-infra/charts/-/tree/main/gitlab/packagecloud), and also to update gitlab-helmfiles where we [reference the chart](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/7bd4690b4226f9158211d3dad12d5c6340851627/bases/environments/ops.yaml#L105-106).

❌ For IaC (e.g., Terraform), is there any secure static code analysis tools like
([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is
being introduced, please explain why.

We do use `checkov` in
[`gprd`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/55b151f7b1cc0237b8d1291478716c1d44bf661e/.gitlab/ci/main.jsonnet#L102),
however the Packagecloud infrastructure is in `ops` (unsure why `checkov` is only enabled for `gprd`).

✅ If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

Both [GitLab Container
Scanner](https://ops.gitlab.net/gitlab-com/gl-infra/ci-images/-/blob/b006e29e7849218201af1b4c60595e1e18e6be69/.gitlab-ci.yml#L84-99)
and [kics](https://ops.gitlab.net/gitlab-com/gl-infra/ci-images/-/blob/b006e29e7849218201af1b4c60595e1e18e6be69/.gitlab-ci.yml#L101-121) are **enabled**.

## Identity and Access Management

_The items below will be reviewed by the Infrasec team._

✅ Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

- Service Account + Workload Identity for Cloud SQL proxy to have access to Cloud SQL (MySQL)
- User accounts and API tokens are stored Cloud SQL (MySQL)
- These are used both by automation (e.g., by the Delivery
team to push packages) and by GitLab Team Members (to access <https://packages.gitlab.com> as users or admins)

✅ Was effort put in to ensure that the new service follows the [least privilege
principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as
possible?

Yes.

❌ Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud
provider)?

Currently only the Cloud SQL proxy has a network policy to allow inbound traffic on port `3306` from the Packagecloud
pods.

[Filed an issue](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24563) to update the network policies as we
are not currently restricting egress traffic from Cloud SQL Proxy, and we do not have a network policy for the Packagecloud pods.

✅ Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

`packages.gitlab.com` is part of the `gitlab.com` Cloudflare zone.

## Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

✅ Did we make an effort to redact customer data from logs?

No customer data in the logs.

✅ What kind of data is stored on each system (secrets, customer data, audit, etc...)?

- Only GitLab packages (RPMs, DEBs) are distributed through packages.gitlab.com
- Only GitLab managed and used credentials are stored in the Cloud SQL (MySQL) database

✅ How is data rated according to our [data classification
  standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is
  RED)?

`GREEN`

✅ Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

Due to the large volume of access logs, these are excluded from Elasticsearch. They are accessible via [Loki](https://dashboards.gitlab.net/goto/rjhk6hGIR?orgId=1).

✅ Ensure appropriate logs are being kept for compliance and requirements for retention are met.

We ship all logs produced by Packagecloud components to the [`gitlab-logs-nonprod` Elasticsearch
deployment](https://nonprod-log.gitlab.net/app/r/s/8CHn8) _except_ for noisy logs that add little value (e.g.,
meaningless Rails metrics logs). We have also enabled shipping of **ALL** logs to
[Loki](https://dashboards.gitlab.net/goto/nq0F0-GSg?orgId=1).

✅ If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

Data classification is `GREEN`.

---

**NOTE**: the _Beta_ and _General Availability_ sections were **removed** as the service has been in production since
circa 2015.
